const getCube = Math.pow(2, 3)
console.log(`The cube of 2 is ${getCube}`);

const address = [ "258 Washington Ave NW", "California", 90011];
const [streetName, state, zipCode] = address;
console.log(`I live at ${streetName} ${state} ${zipCode}`);

const animal = {
	animalName: "Lolong",
	animalWeight: 1075,
	animalMeasure: {
		ft: 20,
		in: 3
	}
}

const {animalName, animalWeight, animalMeasure} = animal;

function getAnimal ({animalName, animalWeight, animalMeasure}){
	console.log(`${animalName} was a saltwater crocodile. He weight ${animalWeight} kgs with a measurement of ${animalMeasure.ft} ft ${animalMeasure.in} in.`)
}

getAnimal(animal);


class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);